package com.example.tp2;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class WineActivity extends Activity {
    WineDbHelper wineDb;
    Wine wine;
    EditText editerNomWine;
    EditText editerRegionWine;
    EditText editerLocalizationWine;
    EditText editPlantedAreaWine;
    EditText editerClimateRegion;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wine);
        wineDb = new WineDbHelper(this);
        Intent recupInfo= getIntent();
        wine =  recupInfo.getParcelableExtra("nomVin");
        editerNomWine= findViewById(R.id.wineName);
        editerRegionWine =  findViewById(R.id.editWineRegion);
        editerLocalizationWine =  findViewById(R.id.editLoc);
        editPlantedAreaWine =  findViewById(R.id.editPlantedArea);
        editerClimateRegion =  findViewById(R.id.editClimate);
        if(wine!=null) {
            editerNomWine.setText(wine.getTitle());
            editerRegionWine.setText(wine.getRegion());
            editerLocalizationWine.setText(wine.getLocalization());
            editPlantedAreaWine.setText(wine.getPlantedArea());
            editerClimateRegion.setText(wine.getClimate());
        }

        Button save = findViewById(R.id.button);

        save.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if(wine!=null) {

                        wine.setTitle(editerNomWine.getText().toString());
                        wine.setClimate(editerClimateRegion.getText().toString());
                        wine.setPlantedArea(editPlantedAreaWine.getText().toString());
                        wine.setRegion(editerRegionWine.getText().toString());
                        wine.setLocalization(editerLocalizationWine.getText().toString());
                        int res = wineDb.updateWine(wine);
                        if(res>0)Toast.makeText(WineActivity.this, "Enregistré", Toast.LENGTH_LONG).show();


                }
                else{
                    Wine wine = new Wine(editerNomWine.getText().toString(), editerRegionWine.getText().toString(), editerLocalizationWine.getText().toString(), editerClimateRegion.getText().toString(),  editPlantedAreaWine.getText().toString());
                    boolean insert = wineDb.addWine(wine);
                    if(insert) Toast.makeText(WineActivity.this, "Enregistré", Toast.LENGTH_LONG).show();
                }


            }

        });
    }


}