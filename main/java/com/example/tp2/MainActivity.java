package com.example.tp2;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.ContextMenu;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;


public class MainActivity extends AppCompatActivity implements View.OnCreateContextMenuListener {
    WineDbHelper wineDB;
    ListView listeVin;
    SimpleCursorAdapter adapteur;
    Cursor fetch;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        wineDB = new WineDbHelper(this) ;

        fetch= wineDB.fetchAllWines();

        adapteur= new SimpleCursorAdapter(this,android.R.layout.simple_list_item_2,fetch,new String[]{WineDbHelper.COLUMN_NAME, WineDbHelper.COLUMN_WINE_REGION},new int[]{android.R.id.text1, android.R.id.text2},0);
        listeVin=findViewById(R.id.wineList);
        listeVin.setAdapter(adapteur);
        listeVin.setOnCreateContextMenuListener(this);
        listeVin.invalidateViews();

        listeVin.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView parent, View v, int position, long id) {
                Intent intent = new Intent(MainActivity.this, WineActivity.class);
                Cursor item = (Cursor) parent.getItemAtPosition(position);
                Wine wine = WineDbHelper.cursorToWine(item);
                wine.setId(id);
                intent.putExtra("nomVin", wine);
                startActivity(intent);
            }

        });
        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, WineActivity.class);
                startActivity(intent);
            }
        });

    }
    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        menu.add(0, v.getId(), 0, "Supprimer");

    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item
                .getMenuInfo();
        AdapterView parent = (AdapterView)item.getActionView();
            int position = info.position;
        Cursor wineCursor = (Cursor) adapteur.getItem(position);
        boolean deleted = wineDB.deleteWine(wineCursor);
            if(deleted){

                Snackbar.make(info.targetView, "Vin Supprimé", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
                fetch= wineDB.fetchAllWines();
                adapteur= new SimpleCursorAdapter(this,android.R.layout.simple_list_item_2,fetch,new String[]{WineDbHelper.COLUMN_NAME, WineDbHelper.COLUMN_WINE_REGION},new int[]{android.R.id.text1, android.R.id.text2},0);
                listeVin.setAdapter(adapteur);
            }
            else{
                Snackbar.make(info.targetView, "Erreur", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
